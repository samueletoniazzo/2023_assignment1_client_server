import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Classe utilizzata dal client per instaurare la connessione con il server.
 */
public final class SocketClient {

    /**
     * metodo Main, ill primo ad essere eseguito dal Client, dove
     * avviene la connessione e lo scambio di informazioni tra client e server.
     * @param args
     * @throws UnknownHostException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static void main(final String[] args)
            throws UnknownHostException, IOException, ClassNotFoundException,
            InterruptedException {
        //get the localhost IP address,
        //if server is running on some other IP, you need to use that
        InetAddress host = InetAddress.getLocalHost();
        Socket socket = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;

        int count = Integer.parseInt("10");

        for (int i = 0; i < count; i++) {
            //establish socket connection to server
            socket = new Socket(host.getHostName(), Integer.parseInt("9876"));
            //write to socket using ObjectOutputStream
            oos = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Sending request to Socket Server");

            if (i == Integer.parseInt("9")) {
                String message = "exit";
                oos.writeInt(message.length()); // write length of the message
                oos.write(message.getBytes());
            } else {
                oos.writeInt((i + "").length()); // write length of the message
                oos.write((i + "").getBytes());
            }

            //read the server response message
            ois = new ObjectInputStream(socket.getInputStream());

            String message;

            int length = ois.readInt();
            if (length > 0) {
                byte[] byteMessage = new byte[length];
                ois.readFully(byteMessage, 0, byteMessage.length);

                message = new String(byteMessage);
                System.out.println("Message: " + message);
            }
            //close resources
            ois.close();
            oos.close();
            Thread.sleep(Integer.parseInt("100"));
        }
    }

    /**
     * default private constructor.
     */
    private SocketClient() {
        throw new AssertionError();
    }

}
