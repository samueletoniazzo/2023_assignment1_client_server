# 2023 assignment1 Client Server

## Gruppo CSG


## **Membri**


- **Claudio Menegotto (Mat. 918953)**
- **Samuele Toniazzo (Mat. 918624)**
- **Giacomo Colaprico (Mat. 920829)**

## Ruoli

**Samuele** e **Giacomo** si sono occupati principalmente della creazione degli script per la pipeline,
mentre **Claudio** ha provveduto alla scrittura del codice sorgente del progetto e della creazione della struttura di esso. Tutti quanti hanno contribuito alla risoluzione di eventuali errori durante l’intero sviluppo.

## Repository URL


repository 1: [https://gitlab.com/Mene-hub/2023_assignment1_client_server](https://gitlab.com/Mene-hub/2023_assignment1_client_server)

repository 2: [https://gitlab.com/samueletoniazzo/2023_assignment1_client_server](https://gitlab.com/samueletoniazzo/2023_assignment1_client_server)

## Descrizione


Come progetto abbiamo optato per una semplice applicazione Client- Server in cui questi comunicano in maniera automatica tramite **Socket** dei semplici Hello client. Nella repository sono presenti due directory principali contenti una il codice sorgente di Client e una di Server in cui sono presenti in ciascuna un file `POM.xml`. I due moduli sono dipendenti da un file `POM.xml` padre presente alla radice del progetto in cui vengono definiti plugin in comune per entrambi i moduli. Nel file  `POM.xml` padre sono presenti i plugin validi per l’intero progetto mentre nei  `POM.xml` dei due moduli sono presenti i plugin relativi ai singoli moduli come il plugin per la creazione dei package.

## Tecnologie Utilizzate


- **Java**
- **IntelliJ**
- **GitLab**
- **Maven**

## Stage


Nel nostro progetto al momento abbiamo implementato con successo quasi tutti gli stage ad eccezione della release che non è ancora stata implementata e la **verify** che è stata implementata ma da alcuni problemi come spiegati in seguito. Da notare che la cache è stata inserita nello script della nostra pipeline ma non ancora eventualmente implementata vedremo successivamente se sarà necessario per velocizzare alcuni stage. 

> **Build**
Questo stage della pipeline è responsabile della compilazione del progetto. In questa fase il codice sorgente viene trasformato in un formato utilizzabile o eseguibile tramite il comando riservata `mvn compile`
> 

> **Verify**
Questo stage si concentra sulla verifica della qualità del codice e sull'individuazione di potenziali problemi o violazioni delle regole di stile nel codice sorgente. Nella nostra pipeline abbiamo implementato due job uno usando C**heckstyle** e uno usando S**potbugs** implementando i rispettivi plugin nel file `POM.xml`. Questi due Job hanno creato dei problemi in fase di esecuzione fallendo spesso o generando warning trovando eventuali regole di stile nel codice nel caso di C**heckstyle** o problemi di *deserializzazione* nel caso di S**potbugs**. La configurazione `allowfailure:true` è solo momentanea e indica che la fase può generare errori  senza interrompere l'intera pipeline.
> 

> **Unit-Test**
Gli **unit-test** verificano che parti specifiche del codice funzionino correttamente, individuando errori in modo tempestivo attraverso il comando riservato `mvn test`
> 

> **Integration-Test**
Gli **integration test**, si concentrano sull'interazione e sulla collaborazione tra diverse parti o componenti di un'applicazione o del sistema nel suo complesso. Nel nostro caso abbiamo creato deli file per simulare l’interazione tra client e server tipo una simulazione di connessione al server da parte del client, e il tentativo di inviare dati al server. Vengono gestiti tramite il comando riservatao`mvn verify`
> 

> **Package**
fase in cui tramite il comando `mvn clean package` si pulisce il progetto da eventuali risultati di build precedenti e quindi esegue una nuova build, compilandola e creando il pacchetto finale generalmente di tipo `.Jar` come nel nostro progetto in seguito alla corretta implementazione del plugin dedicato. La configurazione `allowfailure:true` è solo momentanea e indica che la fase può generare errori  senza interrompere l'intera pipeline.
> 

> **Docs**
questa fase di una pipeline è responsabile della generazione della documentazione del progetto tramite il comando `mvn javadoc:javadoc`**.** Abbiamo riscontrato problemi iniziali nella realizzazione di questa fase poiché venivano generati problemi riguardanti la JAVA_HOME, risolti successivamente inserendo l’image all’inizio della pipeline e rimuovendo `before-script` in cui veniva installato **maven** prima di ogni script poiché assieme erano in conflitto.
> 

> **Release**
questa fase non è ancora stata implementata nella pipeline e ci stiamo muovendo per capire come gestire al meglio i `dockerfile`.
> 

## Problematiche riscontrate durante lo sviluppo


> **********************************Problematiche di esecuzione**********************************
Durante l’esecuzione abbiamo verificato che il client ed il server non comunicano correttamente, poiché il codice originale viene riconosciuto con un livello di gravità alto.
> 

> **Verify
Checkstyle** e **Spotbugs** funzionano correttamente durante l’esecuzione della pipeline, ma non vengono ancora esportati i relativi report
> 

> **Package**
lo stage di Package non genera ancora correttamente i due file `SocketClient.jar` e `ServerSocket.jar` per questo motivo è stato inserito il comando `allowfailure:true` per evitare di bloccare l’intera pipeline.
> 

> **********Docs**********
La fase di documentazione avviene in modo corretto, le pagine generate però non sono ancora state pubblicate in modo pubblico su **gitlab**.
>
