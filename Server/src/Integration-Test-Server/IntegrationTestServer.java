import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ServerIntegrationTest {

    @Test
    public void testServerIntegration() {
        // Simulazione di accettazione di una connessione dal client
        Server server = new Server();
        assertTrue(server.acceptConnection());

        // Simulazione di ricezione di dati dal client
        String clientData = server.receiveData();
        assertEquals("Test data", clientData);

        // Simulazione di invio di una risposta al client
        assertTrue(server.sendResponse("Expected response"));

        // Gestione di uno scenario di errore (simulato)
        assertFalse(server.receiveData().equals("Invalid data"));
    }
}
