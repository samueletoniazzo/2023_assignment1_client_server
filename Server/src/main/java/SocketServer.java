import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Classe utilizzata dal server in attesa che * un client invii delle informazioni.
 * Questa classe gestisce la connessione tra il  * client e il server sulla porta specificata.
 * La porta è statica e di tipo intero (int).
 */
public final class SocketServer {

    /**
     * Socket utilizzato dal server in attesa della richiesta di connessione.
     */
    private static ServerSocket server;

    /**
     * port sulla quale il Server rimane in attesa.
     */
    private static int port = Integer.parseInt("9876");

    /**
     * metodo Main, il primo ad essere eseguito dal Server,
     * in questo metodo il server resta in attesa del
     * client e continu la comunicazione.
     * @param args args
     * @throws IOException IOException
     * @throws ClassNotFoundException ClassNotFoundException
     */
    public static void main(final String[] args)
            throws IOException, ClassNotFoundException {
        //create the socket server object
        server = new ServerSocket(port);
        while (true) {
            System.out.println("Waiting for the client request");
            //creating socket and waiting for client connection
            Socket socket = server.accept();
            //read from socket to ObjectInputStream object
            ObjectInputStream ois =
                    new ObjectInputStream(socket.getInputStream());
            //create ObjectOutputStream object
            ObjectOutputStream oos =
                    new ObjectOutputStream(socket.getOutputStream());

            String message = "";

            int length = ois.readInt();
            if (length > 0) {
                byte[] byteMessage = new byte[length];
                ois.readFully(byteMessage, 0, byteMessage.length);

                message = new String(byteMessage);
                System.out.println("Message Received: " + message);

                //write object to Socket
                oos.writeInt(("Hi Client " + message).length());
                oos.write(("Hi Client " + message).getBytes());
            }
            //close resources
            ois.close();
            oos.close();
            socket.close();
            //terminate the server if client sends exit request
            if (message.equalsIgnoreCase("exit")) {
                break;
            }
        }
        System.out.println("Shutting down Socket server!!");
        //close the ServerSocket object
        server.close();
    }

    /**
     * default private constructor.
     */
    private SocketServer() {
        throw new AssertionError();
    }
}
